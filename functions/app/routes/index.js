module.exports = {
	Auth: require('./auth'),
	Users: require('./users'),
	Languages: require('./languages'),
	Money: require('./money'),
	Applications: require('./applications'),
	Teachers: require('./teachers')
}