import React, { Component } from 'react';
import {
	Typography,
	FormControl,
	InputLabel,
	Select,
	MenuItem,
	Input
} from '@material-ui/core';

import countries from './countries';

class SelectLocalities extends Component {
	state = {
    country: '',
    state: '',
    stateObject: null
	}
	componentWillUpdate(prevProps, prevState, snapshot){
    if('value' in prevProps){
      if(prevProps.value !== undefined || prevProps.value !== null){
        if(prevProps.value.country !== prevState.country && prevProps.value.state !== prevState.state){
          let object = countries.find(docs => docs.country === prevProps.value.country)
          if(typeof object === 'object')
            this.setState({
              country: prevProps.value.country,
              state: prevProps.value.state,
              stateObject: object.states
            })
          else 
            this.setState({
              country: prevProps.value.country,
              state: prevProps.value.state,
              stateObject: null
            })
        }
      }
    }
  }
	handleChangeState = name => event => {
		this.setState({
			[name]: event.target.value
    })
    this.handleChange(this.state.country, event.target.value)
	}
	handleChangeContries = event => {
		let object = countries.find(docs =>
			docs.country === event.target.value
		)
		this.setState({
			country: event.target.value,
			stateObject: object.states
    })
    this.handleChange(event.target.value, null)
  }
  handleChange(country = null, state = null) {
    this.props.onChange({
      target: {
        value: {
          country: country,
          state: state
        }
      }
    })
  }
	render() {
		const { label, disabled, className, required } = this.props;
		return (
			<div>
        <Typography variant="body2" style={{marginTop: 10}}>{label}</Typography>
				<FormControl 
					required={required}
					fullWidth 
          margin="normal"
          style={{
            // marginTop: 10,
            // marginLeft: '2%',
            // width: '98%'
					}}
					disabled={disabled}
					className={className}
        >
					<InputLabel htmlFor="country">Pais</InputLabel>
					<Select
						value={this.state.country ? this.state.country : '' }
						onChange={event => this.handleChangeContries(event)}
						input={<Input name="country" id="country"/>}
					>
						{
							countries.map(docs => 
								<MenuItem key={docs.country} value={docs.country}>
									{docs.country}
								</MenuItem>
							)
						}
					</Select>
				</FormControl>
				<FormControl 
					required={required}
					fullWidth 
          margin="normal"
          style={{
            // marginLeft: '2%',
            // width: '98%'
					}}
					disabled={disabled}
					className={className}
				>
					<InputLabel htmlFor="state">Estado</InputLabel>
					<Select
						value={this.state.state ? this.state.state : '' }
						onChange={this.handleChangeState('state')}
						input={<Input name="state" id="state"/>}
					>
						{
							this.state.stateObject
								?
							this.state.stateObject.map(docs => 
								<MenuItem key={docs} value={docs}>
									{docs}
								</MenuItem>
							)
								:
							''
						}
					</Select>
				</FormControl>
			</div>
		)
	}
}

export default SelectLocalities;